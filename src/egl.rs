use libc::c_int;
use client::wl_surface;

#[repr(C)]
pub struct wl_egl_window;
impl Copy for wl_egl_window {}

#[link(name = "wayland-egl")]
extern "C" {
	pub fn wl_egl_window_create(surface: *mut  wl_surface, width: c_int, height: c_int) -> *mut wl_egl_window;
	pub fn wl_egl_window_destroy(egl_window: *mut wl_egl_window);
	pub fn wl_egl_window_resize(egl_window: *mut wl_egl_window, width: c_int, height: c_int, dx: c_int, dy: c_int);
	pub fn wl_egl_window_get_attached_size(egl_window: *mut wl_egl_window, width: *mut c_int, height: *mut c_int);
}
