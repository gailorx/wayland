pub const WAYLAND_VERSION_MAJOR: i32 = 1;
pub const WAYLAND_VERSION_MINOR: i32 = 6;
pub const WAYLAND_VERSION_MICRO: i32 = 0;
pub const WAYLAND_VERSION: &'static str = "1.6.0";