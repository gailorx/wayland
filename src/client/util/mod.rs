#![allow(non_camel_case_types)]

use libc::{c_int, c_char, c_void};

// safe but inefficient version of c macro offsetof
macro_rules! __offsetof(
    ($struct_type:ty, $field_name:ident) => (
        {
            let intstance: $struct_type = unsafe { ::std::mem::zeroed() };
            (&intstance.$field_name as *const _ as usize) - (&intstance as *const $struct_type as usize)
        }
    )
);

// Requires an unsafe block
// A little different to c version
// Takes the address of the field, field_address
// Takes the Type of the struct it belongs to
// Takes the identifier of the field
#[macro_export]
macro_rules! wl_container_of(
    ($field_address: expr, $struct_type: ty, $field_name:ident) => (
        {
            ::std::intrinsics::offset($field_address, -__offsetof!($struct_type, $field_name) as isize) as *mut $struct_type
        }
    )
);

mod wl_argument_mod;
mod wl_array_mod;
mod wl_fixed_t_mod;
mod wl_interface_mod;
mod wl_list_mod;
mod wl_message_mod;
mod wl_object_mod;

pub use self::wl_argument_mod::*;
pub use self::wl_array_mod::*;
pub use self::wl_fixed_t_mod::*;
pub use self::wl_interface_mod::*;
pub use self::wl_list_mod::*;
pub use self::wl_message_mod::*;
pub use self::wl_object_mod::*;

pub type wl_dispatcher_func_t = Option<extern "C" fn(arg1: *const c_void,
                               arg2: *mut c_void, arg3: u32,
                               arg4: *const wl_message,
                               arg5: *mut wl_argument)
                              -> c_int>;
pub type wl_log_func_t = Option<extern "C" fn(arg1: *const c_char, ...) -> ()>;


#[link(name = "wayland-client")]
extern "C" {
    pub fn wl_log_set_handler_client(handler: wl_log_func_t);
}
