use std::{default, mem};
use libc::c_char;
use client::util::wl_interface_mod::wl_interface;

#[repr(C)]
pub struct wl_message {
    pub name: *const c_char,
    pub signature: *const c_char,
    pub types: *mut *const wl_interface,
}
impl default::Default for wl_message {
    fn default() -> wl_message { unsafe { mem::zeroed() } }
}
impl Copy for wl_message {}
