use std::{default, mem};
use libc::c_char;
use client::util::wl_fixed_t_mod::wl_fixed_t;
use client::util::wl_array_mod::wl_array;
use client::util::wl_object_mod::wl_object;

#[repr(C)]
pub struct wl_argument {
    pub __data: [u64; 1us],
}
impl wl_argument {
    pub unsafe fn i(&mut self) -> *mut i32 {
        mem::transmute(&self.__data)
    }
    pub unsafe fn u(&mut self) -> *mut u32 {
        mem::transmute(&self.__data)
    }
    pub unsafe fn f(&mut self) -> *mut wl_fixed_t {
        mem::transmute(&self.__data)
    }
    pub unsafe fn s(&mut self) -> *mut *const c_char {
        mem::transmute(&self.__data)
    }
    pub unsafe fn o(&mut self) -> *mut *mut wl_object {
        mem::transmute(&self.__data)
    }
    pub unsafe fn n(&mut self) -> *mut u32 {
        mem::transmute(&self.__data)
    }
    pub unsafe fn a(&mut self) -> *mut *mut wl_array {
        mem::transmute(&self.__data)
    }
    pub unsafe fn h(&mut self) -> *mut i32 {
        mem::transmute(&self.__data)
    }
}
impl default::Default for wl_argument {
    fn default() -> wl_argument { unsafe { mem::zeroed() } }
}
impl Copy for wl_argument {}
