use std::{default, mem};
use libc::{c_int, c_char};
use client::util::wl_message_mod::wl_message;

#[repr(C)]
pub struct wl_interface {
    pub name: *const c_char,
    pub version: c_int,
    pub method_count: c_int,
    pub methods: *const wl_message,
    pub event_count: c_int,
    pub events: *const wl_message,
}
impl default::Default for wl_interface {
    fn default() -> wl_interface { unsafe { mem::zeroed() } }
}
impl Copy for wl_interface {}
