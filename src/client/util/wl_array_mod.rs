use std::{default, mem};
use libc::{size_t, c_int, c_void};

#[repr(C)]
pub struct wl_array {
    pub size: size_t,
    pub alloc: size_t,
    pub data: *mut c_void,
}
impl default::Default for wl_array {
    fn default() -> wl_array { unsafe { mem::zeroed() } }
}
impl Copy for wl_array {}


// Closures don't work because they will be called from a C abi function, which makes compiler fail.
// Not sure if it should be allowed or not.
pub unsafe fn wl_array_for_each<T, F>(start_position: *mut T, array: *const wl_array, mut function: F)
                            where F: FnMut(*mut T) -> () {

    while (start_position as *mut u8 as usize) < ((*array).data.offset((*array).size as isize) as *mut u8 as usize) {
        function(start_position);
            start_position.offset(1);
    }
}

/* Macro version
#[macro_export]
macro_rules! wl_array_for_each(
    ($position:expr, $array:expr, $func:block) => (
        while ($position as *mut u8 as usize) < ((*$array).data.offset((*$array).size as isize) as *mut u8 as usize) {
            $func;
            $position.offset(1);``
        }
    )
);*/



#[link(name = "wayland-client")]
extern "C" {
    pub fn wl_array_init(array: *mut wl_array);
    pub fn wl_array_release(array: *mut wl_array);
    pub fn wl_array_add(array: *mut wl_array, size: size_t) -> *mut c_void;
    pub fn wl_array_copy(array: *mut wl_array, source: *mut wl_array) -> c_int;
}
