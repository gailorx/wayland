use std::{default, mem};
use libc::c_int;

#[repr(C)]
pub struct wl_list {
    pub prev: *mut wl_list,
    pub next: *mut wl_list,
}
impl default::Default for wl_list {
    fn default() -> wl_list { unsafe { mem::zeroed() } }
}
impl Copy for wl_list {}


    // TODO Macros for these!
    /*pub fn wl_list_for_each(pos, head, member) {
        for (pos = wl_container_of((head)->next, pos, member);
            &pos->member != (head);
            pos = wl_container_of(pos->member.next, pos, member))
    }

    pub fn wl_list_for_each_safe(pos, tmp, head, member) {
        for (pos = wl_container_of((head)->next, pos, member),
            tmp = wl_container_of((pos)->member.next, tmp, member);
            &pos->member != (head);
            pos = tmp,
            tmp = wl_container_of(pos->member.next, tmp, member))
    }

    pub fn wl_list_for_each_reverse(pos, head, member) {
        for (pos = wl_container_of((head)->prev, pos, member);
            &pos->member != (head);
            pos = wl_container_of(pos->member.prev, pos, member))
    }

    pub fn wl_list_for_each_reverse_safe(pos, tmp, head, member) {
        for (pos = wl_container_of((head)->prev, pos, member),
            tmp = wl_container_of((pos)->member.prev, tmp, member);
            &pos->member != (head);
            pos = tmp,
            tmp = wl_container_of(pos->member.prev, tmp, member))
    }
*/

#[link(name = "wayland-client")]
extern "C" {
    pub fn wl_list_init(list: *mut wl_list);
    pub fn wl_list_insert(list: *mut wl_list, elm: *mut wl_list);
    pub fn wl_list_remove(elm: *mut wl_list);
    pub fn wl_list_length(list: *const wl_list) -> c_int;
    pub fn wl_list_empty(list: *const wl_list) -> c_int;
    pub fn wl_list_insert_list(list: *mut wl_list, other: *mut wl_list);
}
