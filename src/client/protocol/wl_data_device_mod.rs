use std::{default, mem};
use libc::{c_int, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy,wl_proxy_add_listener,  wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::protocol::wl_data_offer_mod::wl_data_offer;
use client::protocol::wl_data_source_mod::wl_data_source;
use client::protocol::wl_surface_mod::wl_surface;
use client::util::{wl_fixed_t, wl_interface};

#[link(name = "wayland-client")]
extern "C" {
    pub static wl_data_device_interface: wl_interface;
}

const WL_DATA_DEVICE_START_DRAG: u32 = 0;
const WL_DATA_DEVICE_SET_SELECTION: u32 = 1;


#[repr(C)]
pub struct wl_data_device;
impl Copy for wl_data_device {}


#[repr(C)]
pub struct wl_data_device_listener {
    pub data_offer: Option<extern "C" fn(data: *mut c_void, wl_data_device: *mut wl_data_device, id: *mut wl_data_offer) -> ()>,
    pub enter: Option<extern "C" fn(data: *mut c_void, wl_data_device: *mut wl_data_device, serial: u32, surface: *mut wl_surface, x: wl_fixed_t, y: wl_fixed_t, id: *mut wl_data_offer) -> ()>,
    pub leave: Option<extern "C" fn(data: *mut c_void, wl_data_device: *mut wl_data_device) -> ()>,
    pub motion: Option<extern "C" fn(data: *mut c_void, wl_data_device: *mut wl_data_device, time: u32, x: wl_fixed_t, y: wl_fixed_t) -> ()>,
    pub drop: Option<extern "C" fn(data: *mut c_void, wl_data_device: *mut wl_data_device) -> ()>,
    pub selection: Option<extern "C" fn(data: *mut c_void, wl_data_device: *mut wl_data_device, id: *mut wl_data_offer) -> ()>,
}
impl Copy for wl_data_device_listener {}
impl default::Default for wl_data_device_listener {
    fn default() -> wl_data_device_listener {
        unsafe { mem::zeroed() }
    }
}

#[inline]
pub unsafe fn wl_data_device_add_listener(data_device: *mut wl_data_device, listener: *const wl_data_device_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(data_device as *mut wl_proxy, mem::transmute(listener), data)
}

#[inline]
pub unsafe fn wl_data_device_set_user_data(data_device: *mut wl_data_device, user_data: *mut c_void) {
    wl_proxy_set_user_data(data_device as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_data_device_get_user_data(data_device: *mut wl_data_device) -> *mut c_void {
    wl_proxy_get_user_data(data_device as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_data_device_destroy(data_device: *mut wl_data_device) {
    wl_proxy_destroy(data_device as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_data_device_start_drag(data_device: *mut wl_data_device, source: *mut wl_data_source, origin: *mut wl_surface, icon: *mut wl_surface, serial: i32) {
    wl_proxy_marshal(data_device as *mut wl_proxy, WL_DATA_DEVICE_START_DRAG, source, origin, icon, serial);
}

#[inline]
pub unsafe fn wl_data_device_set_selection(data_device: *mut wl_data_device, source: *mut wl_data_source, serial: i32) {
    wl_proxy_marshal(data_device as *mut wl_proxy, WL_DATA_DEVICE_SET_SELECTION, source, serial);
}
