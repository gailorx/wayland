use std::{default, mem};
use libc::{c_int, c_uint, c_char, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::protocol::wl_output_mod::wl_output;
use client::protocol::wl_surface_mod::wl_surface;
use client::protocol::wl_seat_mod::wl_seat;
use client::util::wl_interface;

#[link(name = "wayland-client")]
extern "C" {
    pub static wl_shell_surface_interface: wl_interface;
}


pub type wl_shell_surface_resize = c_uint;
pub const WL_SHELL_SURFACE_RESIZE_NONE: wl_shell_surface_resize = 0;
pub const WL_SHELL_SURFACE_RESIZE_TOP: wl_shell_surface_resize = 1;
pub const WL_SHELL_SURFACE_RESIZE_BOTTOM: wl_shell_surface_resize = 2;
pub const WL_SHELL_SURFACE_RESIZE_LEFT: wl_shell_surface_resize = 4;
pub const WL_SHELL_SURFACE_RESIZE_TOP_LEFT: wl_shell_surface_resize = 5;
pub const WL_SHELL_SURFACE_RESIZE_BOTTOM_LEFT: wl_shell_surface_resize = 6;
pub const WL_SHELL_SURFACE_RESIZE_RIGHT: wl_shell_surface_resize = 8;
pub const WL_SHELL_SURFACE_RESIZE_TOP_RIGHT: wl_shell_surface_resize = 9;
pub const WL_SHELL_SURFACE_RESIZE_BOTTOM_RIGHT: wl_shell_surface_resize = 10;

pub type wl_shell_surface_transient = c_uint;
pub const WL_SHELL_SURFACE_TRANSIENT_INACTIVE: wl_shell_surface_transient = 1;

pub type wl_shell_surface_fullscreen_method = c_uint;
pub const WL_SHELL_SURFACE_FULLSCREEN_METHOD_DEFAULT: wl_shell_surface_fullscreen_method = 0;
pub const WL_SHELL_SURFACE_FULLSCREEN_METHOD_SCALE: wl_shell_surface_fullscreen_method = 1;
pub const WL_SHELL_SURFACE_FULLSCREEN_METHOD_DRIVER: wl_shell_surface_fullscreen_method = 2;
pub const WL_SHELL_SURFACE_FULLSCREEN_METHOD_FILL: wl_shell_surface_fullscreen_method = 3;

pub const WL_SHELL_SURFACE_PONG: u32 = 0;
pub const WL_SHELL_SURFACE_MOVE: u32 = 1;
pub const WL_SHELL_SURFACE_RESIZE: u32 = 2;
pub const WL_SHELL_SURFACE_SET_TOPLEVEL: u32 = 3;
pub const WL_SHELL_SURFACE_SET_TRANSIENT: u32 = 4;
pub const WL_SHELL_SURFACE_SET_FULLSCREEN: u32 = 5;
pub const WL_SHELL_SURFACE_SET_POPUP: u32 = 6;
pub const WL_SHELL_SURFACE_SET_MAXIMIZED: u32 = 7;
pub const WL_SHELL_SURFACE_SET_TITLE: u32 = 8;
pub const WL_SHELL_SURFACE_SET_CLASS: u32 = 9;


#[repr(C)]
pub struct wl_shell_surface;
impl Copy for wl_shell_surface {}


#[repr(C)]
pub struct wl_shell_surface_listener {
    pub ping: Option<extern "C" fn(data: *mut c_void, wl_shell_surface: *mut wl_shell_surface, serial: u32) -> ()>,
    pub configure: Option<extern "C" fn(data: *mut c_void, wl_shell_surface: *mut wl_shell_surface, edges: u32, width: i32, height: i32) -> ()>,
    pub popup_done: Option<extern "C" fn(data: *mut c_void, wl_shell_surface: *mut wl_shell_surface) -> ()>,
}
impl Copy for wl_shell_surface_listener {}
impl default::Default for wl_shell_surface_listener {
    fn default() -> wl_shell_surface_listener {
        unsafe { mem::zeroed() }
    }
}

#[inline]
pub unsafe fn wl_shell_surface_add_listener(shell_surface: *mut wl_shell_surface, listener: *const wl_shell_surface_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(shell_surface as *mut wl_proxy, mem::transmute(listener), data)
}

#[inline]
pub unsafe fn wl_shell_surface_set_user_data(shell_surface: *mut wl_shell_surface, user_data: *mut c_void) {
    wl_proxy_set_user_data(shell_surface as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_shell_surface_get_user_data(shell_surface: *mut wl_shell_surface) -> *mut c_void {
    wl_proxy_get_user_data(shell_surface as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_shell_surface_destroy(shell_surface: *mut wl_shell_surface) {
    wl_proxy_destroy(shell_surface as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_shell_surface_pong(shell_surface: *mut wl_shell_surface, serial: i32) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_PONG, serial);
}

#[inline]
pub unsafe fn wl_shell_surface_move(shell_surface: *mut wl_shell_surface, seat: *mut wl_seat, serial: i32) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_MOVE, seat, serial);
}

#[inline]
pub unsafe fn wl_shell_surface_resize(shell_surface: *mut wl_shell_surface, seat: *mut wl_seat, serial: i32, edges: u32) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_RESIZE, seat, serial, edges);
}

#[inline]
pub unsafe fn wl_shell_surface_set_toplevel(shell_surface: *mut wl_shell_surface) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_SET_TOPLEVEL);
}

#[inline]
pub unsafe fn wl_shell_surface_set_transient(shell_surface: *mut wl_shell_surface, parent: *mut wl_surface, x: i32, y: i32, flags: u32) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_SET_TRANSIENT, parent, x, y, flags);
}

#[inline]
pub unsafe fn wl_shell_surface_set_fullscreen(shell_surface: *mut wl_shell_surface, method: u32, framerate: u32, output: *mut wl_output) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_SET_FULLSCREEN, method, framerate, output);
}

#[inline]
pub unsafe fn wl_shell_surface_set_popup(shell_surface: *mut wl_shell_surface, seat: *mut wl_seat, serial: i32, parent: *mut wl_surface, x: i32, y: i32, flags: u32) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_SET_POPUP, seat, serial, parent, x, y, flags);
}

#[inline]
pub unsafe fn wl_shell_surface_set_maximized(shell_surface: *mut wl_shell_surface, output: *mut wl_output) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_SET_MAXIMIZED, output);
}

#[inline]
pub unsafe fn wl_shell_surface_set_title(shell_surface: *mut wl_shell_surface, title: *const c_char) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_SET_TITLE, title);
}

#[inline]
pub unsafe fn wl_shell_surface_set_class(shell_surface: *mut wl_shell_surface, class_: *const c_char) {
    wl_proxy_marshal(shell_surface as *mut wl_proxy, WL_SHELL_SURFACE_SET_CLASS, class_);
}
