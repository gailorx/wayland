use std::{default, mem};
use libc::{c_int, c_char, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_data_offer_interface: wl_interface;
}

const WL_DATA_OFFER_ACCEPT: u32 = 0;
const WL_DATA_OFFER_RECEIVE: u32 = 1;
const WL_DATA_OFFER_DESTROY: u32 = 2;


#[repr(C)]
pub struct wl_data_offer;
impl Copy for wl_data_offer {}


#[repr(C)]
pub struct wl_data_offer_listener {
    pub offer: Option<extern "C" fn(data: *mut c_void, wl_data_offer: *mut wl_data_offer,  mime_type: *const c_char) -> ()>,
}
impl Copy for wl_data_offer_listener {}
impl default::Default for wl_data_offer_listener {
    fn default() -> wl_data_offer_listener {
        unsafe { mem::zeroed() }
    }
}


#[inline]
pub unsafe fn wl_data_offer_add_listener(data_offer: *mut wl_data_offer, listener: *const wl_data_offer_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(data_offer as *mut wl_proxy, mem::transmute(listener), data)
}


#[inline]
pub unsafe fn wl_data_offer_set_user_data(data_offer: *mut wl_data_offer, user_data: *mut c_void) {
    wl_proxy_set_user_data(data_offer as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_data_offer_get_user_data(data_offer: *mut wl_data_offer) -> *mut c_void {
    wl_proxy_get_user_data(data_offer as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_data_offer_accept(data_offer: *mut wl_data_offer, serial: i32, mime_type: *const c_char) {
    wl_proxy_marshal(data_offer as *mut wl_proxy, WL_DATA_OFFER_ACCEPT, serial, mime_type);
}

#[inline]
pub unsafe fn wl_data_offer_receive(data_offer: *mut wl_data_offer, mime_type: *const c_char, fd: i32) {
    wl_proxy_marshal(data_offer as *mut wl_proxy, WL_DATA_OFFER_RECEIVE, mime_type, fd);
}

#[inline]
pub unsafe fn wl_data_offer_destroy(data_offer: *mut wl_data_offer) {
    wl_proxy_marshal(data_offer as *mut wl_proxy, WL_DATA_OFFER_DESTROY);
    wl_proxy_destroy(data_offer as *mut wl_proxy);
}
