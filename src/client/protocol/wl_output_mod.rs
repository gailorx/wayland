use std::{default, mem};
use libc::{c_int, c_uint, c_char, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_destroy};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_output_interface: wl_interface;
}

pub type wl_output_subpixel = c_uint;
pub const WL_OUTPUT_SUBPIXEL_UNKNOWN: wl_output_subpixel = 0;
pub const WL_OUTPUT_SUBPIXEL_NONE: wl_output_subpixel = 1;
pub const WL_OUTPUT_SUBPIXEL_HORIZONTAL_RGB: wl_output_subpixel = 2;
pub const WL_OUTPUT_SUBPIXEL_HORIZONTAL_BGR: wl_output_subpixel = 3;
pub const WL_OUTPUT_SUBPIXEL_VERTICAL_RGB: wl_output_subpixel = 4;
pub const WL_OUTPUT_SUBPIXEL_VERTICAL_BGR: wl_output_subpixel = 5;

pub type wl_output_transform = c_uint;
pub const WL_OUTPUT_TRANSFORM_NORMAL: wl_output_transform = 0;
pub const WL_OUTPUT_TRANSFORM_90: wl_output_transform = 1;
pub const WL_OUTPUT_TRANSFORM_180: wl_output_transform = 2;
pub const WL_OUTPUT_TRANSFORM_270: wl_output_transform = 3;
pub const WL_OUTPUT_TRANSFORM_FLIPPED: wl_output_transform = 4;
pub const WL_OUTPUT_TRANSFORM_FLIPPED_90: wl_output_transform = 5;
pub const WL_OUTPUT_TRANSFORM_FLIPPED_180: wl_output_transform = 6;
pub const WL_OUTPUT_TRANSFORM_FLIPPED_270: wl_output_transform = 7;

pub type wl_output_mode = c_uint;
pub const WL_OUTPUT_MODE_CURRENT: wl_output_mode = 1;
pub const WL_OUTPUT_MODE_PREFERRED: wl_output_mode = 2;


#[repr(C)]
pub struct wl_output;
impl Copy for wl_output {}

#[repr(C)]
pub struct wl_output_listener {
    pub geometry: Option<extern "C" fn(data: *mut c_void, wl_output: *mut wl_output, x: i32, y: i32, physical_width: i32, physical_height: i32, subpixel: i32, make: *const c_char, model: *const c_char, transform: i32) -> ()>,
    pub mode: Option<extern "C" fn(data: *mut c_void, wl_output: *mut wl_output, flags: u32, width: i32, height: i32, refresh: i32) -> ()>,
    pub done: Option<extern "C" fn(data: *mut c_void, wl_output: *mut wl_output) -> ()>,
    pub scale: Option<extern "C" fn(data: *mut c_void,  wl_output: *mut wl_output, factor: i32) -> ()>,
}
impl Copy for wl_output_listener {}
impl default::Default for wl_output_listener {
    fn default() -> wl_output_listener {
        unsafe { mem::zeroed() }
    }
}

#[inline]
pub unsafe fn wl_output_add_listener(output: *mut wl_output, listener: *const wl_output_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(output as *mut wl_proxy, mem::transmute(listener), data)
}

#[inline]
pub unsafe fn wl_output_set_user_data(output: *mut wl_output, user_data: *mut c_void) {
    wl_proxy_set_user_data(output as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_output_get_user_data(output: *mut wl_output) -> *mut c_void {
    wl_proxy_get_user_data(output as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_output_destroy(output: *mut wl_output) {
    wl_proxy_destroy(output as *mut wl_proxy);
}
