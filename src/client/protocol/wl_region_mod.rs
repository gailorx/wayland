use libc::c_void;
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_region_interface: wl_interface;
}

const WL_REGION_DESTROY: u32 = 0;
const WL_REGION_ADD: u32 = 1;
const WL_REGION_SUBTRACT: u32 = 2;


#[repr(C)]
pub struct wl_region;
impl Copy for wl_region {}


#[inline]
pub unsafe fn wl_region_set_user_data(region: *mut wl_region, user_data: *mut c_void) {
    wl_proxy_set_user_data(region as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_region_get_user_data(region: *mut wl_region) -> *mut c_void {
    wl_proxy_get_user_data(region as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_region_destroy(region: *mut wl_region) {
    wl_proxy_marshal(region as *mut wl_proxy, WL_REGION_DESTROY);
    wl_proxy_destroy(region as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_region_add(region: *mut wl_region, x: i32, y: i32, width: i32, height: i32) {
    wl_proxy_marshal(region as *mut wl_proxy, WL_REGION_ADD, x, y, width, height);
}

#[inline]
pub unsafe fn wl_region_subtract(region: *mut wl_region, x: i32, y: i32, width: i32, height: i32) {
    wl_proxy_marshal(region as *mut wl_proxy, WL_REGION_SUBTRACT, x, y, width, height);
}
