use std::{default, mem};
use libc::{c_int, c_char, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_data_source_interface: wl_interface;
}


const WL_DATA_SOURCE_OFFER: u32 = 0;
const WL_DATA_SOURCE_DESTROY: u32 = 1;


#[repr(C)]
pub struct wl_data_source;
impl Copy for wl_data_source {}


#[repr(C)]
pub struct wl_data_source_listener {
    pub target: Option<extern "C" fn(data: *mut c_void, wl_data_source: *mut wl_data_source, mime_type: *const c_char) -> ()>,
    pub send: Option<extern "C" fn(data: *mut c_void, wl_data_source: *mut wl_data_source, mime_type: *const c_char, fd: i32) -> ()>,
    pub cancelled: Option<extern "C" fn(data: *mut c_void, wl_data_source: *mut wl_data_source) -> ()>,
}
impl Copy for wl_data_source_listener {}
impl default::Default for wl_data_source_listener {
    fn default() -> wl_data_source_listener {
        unsafe { mem::zeroed() }
    }
}


#[inline]
pub unsafe fn wl_data_source_add_listener(data_source: *mut wl_data_source, listener: *const wl_data_source_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(data_source as *mut wl_proxy, mem::transmute(listener), data)
}

#[inline]
pub unsafe fn wl_data_source_set_user_data(data_source: *mut wl_data_source, user_data: *mut c_void) {
    wl_proxy_set_user_data(data_source as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_data_source_get_user_data(data_source: *mut wl_data_source) -> *mut c_void {
    wl_proxy_get_user_data(data_source as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_data_source_offer(data_source: *mut wl_data_source, mime_type: *const c_char) {
    wl_proxy_marshal(data_source as *mut wl_proxy, WL_DATA_SOURCE_OFFER, mime_type);
}

#[inline]
pub unsafe fn wl_data_source_destroy(data_source: *mut wl_data_source) {
    wl_proxy_marshal(data_source as *mut wl_proxy, WL_DATA_SOURCE_DESTROY);
    wl_proxy_destroy(data_source as *mut wl_proxy);
}
