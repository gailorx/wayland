#[repr(C)]
pub struct wl_event_queue;
impl Copy for wl_event_queue {}

#[link(name = "wayland-client")]
extern "C" {
    pub fn wl_event_queue_destroy(queue: *mut wl_event_queue);
}
