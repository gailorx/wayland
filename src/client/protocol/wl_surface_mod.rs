use std::{default, ptr, mem};
use libc::{c_uint, c_int, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_marshal_constructor, wl_proxy_destroy};
use client::protocol::wl_callback_mod::{wl_callback, wl_callback_interface};
use client::protocol::wl_region_mod::wl_region;
use client::protocol::wl_buffer_mod::wl_buffer;
use client::protocol::wl_output_mod::wl_output;
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_surface_interface: wl_interface;
}


pub type wl_surface_error = c_uint;
pub const WL_SURFACE_ERROR_INVALID_SCALE: c_uint = 0;
pub const WL_SURFACE_ERROR_INVALID_TRANSFORM: c_uint = 1;

const WL_SURFACE_DESTROY: u32 = 0;
const WL_SURFACE_ATTACH: u32 = 1;
const WL_SURFACE_DAMAGE: u32 = 2;
const WL_SURFACE_FRAME: u32 = 3;
const WL_SURFACE_SET_OPAQUE_REGION: u32 = 4;
const WL_SURFACE_SET_INPUT_REGION: u32 = 5;
const WL_SURFACE_COMMIT: u32 = 6;
const WL_SURFACE_SET_BUFFER_TRANSFORM: u32 = 7;
const WL_SURFACE_SET_BUFFER_SCALE: u32 = 8;


#[repr(C)]
pub struct wl_surface;
impl Copy for wl_surface {}


#[repr(C)]
pub struct wl_surface_listener {
    pub enter: Option<extern "C" fn(data: *mut c_void, wl_surface: *mut wl_surface, output: *mut wl_output) -> ()>,
    pub leave: Option<extern "C" fn(data: *mut c_void, wl_surface: *mut wl_surface, output: *mut wl_output) -> ()>,
}
impl Copy for wl_surface_listener {}
impl default::Default for wl_surface_listener {
    fn default() -> wl_surface_listener {
        unsafe { mem::zeroed() }
    }
}


#[inline]
pub unsafe fn wl_surface_add_listener(surface: *mut wl_surface, listener: *const wl_surface_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(surface as *mut wl_proxy, mem::transmute(listener), data)
}


#[inline]
pub unsafe fn wl_surface_set_user_data(surface: *mut wl_surface, user_data: *mut c_void) {
    wl_proxy_set_user_data(surface as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_surface_get_user_data(surface: *mut wl_surface) -> *mut c_void {
    wl_proxy_get_user_data(surface as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_surface_destroy(surface: *mut wl_surface) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SURFACE_DESTROY);
    wl_proxy_destroy(surface as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_surface_attach(surface: *mut wl_surface, buffer: *mut wl_buffer, x: i32, y: i32) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SURFACE_ATTACH, buffer, x, y);
}

#[inline]
pub unsafe fn wl_surface_damage(surface: *mut wl_surface, x: i32, y: i32, width: i32, height: i32) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SURFACE_DAMAGE, x, y, width, height);
}

#[inline]
pub unsafe fn wl_surface_frame(surface: *mut wl_surface) -> *mut wl_callback {
    wl_proxy_marshal_constructor(surface as *mut wl_proxy, WL_SURFACE_FRAME, &wl_callback_interface, ptr::null_mut::<usize>()) as *mut wl_callback
}

#[inline]
pub unsafe fn wl_surface_set_opaque_region(surface: *mut wl_surface, region: *mut wl_region) {
    wl_proxy_marshal(surface as *mut wl_proxy,
             WL_SURFACE_SET_OPAQUE_REGION, region);
}

#[inline]
pub unsafe fn wl_surface_set_input_region(surface: *mut wl_surface, region: *mut wl_region) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SURFACE_SET_INPUT_REGION, region);
}

#[inline]
pub unsafe fn wl_surface_commit(surface: *mut wl_surface) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SURFACE_COMMIT);
}

#[inline]
pub unsafe fn wl_surface_set_buffer_transform(surface: *mut wl_surface, transform: i32) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SURFACE_SET_BUFFER_TRANSFORM, transform);
}

#[inline]
pub unsafe fn wl_surface_set_buffer_scale(surface: *mut wl_surface, scale: i32) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SURFACE_SET_BUFFER_SCALE, scale);
}
