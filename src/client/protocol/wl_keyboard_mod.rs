use std::{default, mem};
use libc::{c_int, c_uint, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::protocol::wl_surface_mod::wl_surface;
use client::util::{wl_interface, wl_array};


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_keyboard_interface: wl_interface;
}

pub type wl_keyboard_keymap_format = c_uint;
pub const WL_KEYBOARD_KEYMAP_FORMAT_NO_KEYMAP: wl_keyboard_keymap_format = 0;
pub const WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1: wl_keyboard_keymap_format = 1;
pub type wl_keyboard_key_state = c_uint;
pub const WL_KEYBOARD_KEY_STATE_RELEASED: wl_keyboard_key_state = 0;
pub const WL_KEYBOARD_KEY_STATE_PRESSED: wl_keyboard_key_state = 1;


#[repr(C)]
pub struct wl_keyboard;
impl Copy for wl_keyboard {}


#[repr(C)]
pub struct wl_keyboard_listener {
    pub keymap: Option<extern "C" fn(data: *mut c_void, wl_keyboard: *mut wl_keyboard, format: u32, fd: i32, size: u32) -> ()>,
    pub enter: Option<extern "C" fn(data: *mut c_void, wl_keyboard: *mut wl_keyboard, serial: u32, surface: *mut wl_surface, keys: *mut wl_array) -> ()>,
    pub leave: Option<extern "C" fn(data: *mut c_void, wl_keyboard: *mut wl_keyboard, serial: u32, surface: *mut wl_surface) -> ()>,
    pub key: Option<extern "C" fn(data: *mut c_void, wl_keyboard: *mut wl_keyboard, serial: u32, time: u32, key: u32, state: u32) -> ()>,
    pub modifiers: Option<extern "C" fn(data: *mut c_void, wl_keyboard: *mut wl_keyboard, serial: u32, mods_depressed: u32, mods_latched: u32, mods_locked: u32, group: u32) -> ()>,
    pub repeat_info: Option<extern "C" fn(data: *mut c_void, wl_keyboard: *mut wl_keyboard, rate: i32, delay: i32) -> ()>,
}
impl Copy for wl_keyboard_listener {}
impl default::Default for wl_keyboard_listener {
    fn default() -> wl_keyboard_listener {
        unsafe { mem::zeroed() }
    }
}

#[inline]
pub unsafe fn wl_keyboard_add_listener(keyboard: *mut wl_keyboard, listener: *const wl_keyboard_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(keyboard as *mut wl_proxy, mem::transmute(listener), data)
}

pub const WL_KEYBOARD_RELEASE: u32 = 0;

#[inline]
pub unsafe fn wl_keyboard_set_user_data(keyboard: *mut wl_keyboard, user_data: *mut c_void) {
    wl_proxy_set_user_data(keyboard as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_keyboard_get_user_data(keyboard: *mut wl_keyboard) -> *mut c_void {
    wl_proxy_get_user_data(keyboard as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_keyboard_destroy(keyboard: *mut wl_keyboard) {
    wl_proxy_destroy(keyboard as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_keyboard_release(keyboard: *mut wl_keyboard) {
    wl_proxy_marshal(keyboard as *mut wl_proxy, WL_KEYBOARD_RELEASE);
    wl_proxy_destroy(keyboard as *mut wl_proxy);
}
