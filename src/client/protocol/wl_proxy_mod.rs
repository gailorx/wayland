use libc::{c_int, c_char, c_void};
use client::protocol::wl_event_queue_mod::wl_event_queue;
use client::util::{wl_interface, wl_argument, wl_dispatcher_func_t};

#[repr(C)]
pub struct wl_proxy;
impl Copy for wl_proxy {}

#[link(name = "wayland-client")]
extern "C" {
    pub fn wl_proxy_marshal(p: *mut wl_proxy, opcode: u32, ...);
    pub fn wl_proxy_marshal_array(p: *mut wl_proxy, opcode: u32, args: *mut wl_argument);
    pub fn wl_proxy_create(factory: *mut wl_proxy, interface: *const wl_interface) -> *mut wl_proxy;
    pub fn wl_proxy_marshal_constructor(proxy: *mut wl_proxy, opcode: u32, interface: *const wl_interface, ...) -> *mut wl_proxy;
    pub fn wl_proxy_marshal_array_constructor(proxy: *mut wl_proxy, opcode: u32, args: *mut wl_argument, interface: *const wl_interface) -> *mut wl_proxy;
    pub fn wl_proxy_destroy(proxy: *mut wl_proxy);
    pub fn wl_proxy_add_listener(proxy: *mut wl_proxy, implementation: *mut Option<extern "C" fn () -> ()>, data: *mut c_void) -> c_int;
    pub fn wl_proxy_get_listener(proxy: *mut wl_proxy) -> *const c_void;
    pub fn wl_proxy_add_dispatcher(proxy: *mut wl_proxy, dispatcher_func: wl_dispatcher_func_t, dispatcher_data: *const c_void, data: *mut c_void) -> c_int;
    pub fn wl_proxy_set_user_data(proxy: *mut wl_proxy, user_data: *mut c_void);
    pub fn wl_proxy_get_user_data(proxy: *mut wl_proxy) -> *mut c_void;
    pub fn wl_proxy_get_id(proxy: *mut wl_proxy) -> u32;
    pub fn wl_proxy_get_class(proxy: *mut wl_proxy) -> *const c_char;
    pub fn wl_proxy_set_queue(proxy: *mut wl_proxy, queue: *mut wl_event_queue);
}
