use std::{default, mem};
use libc::{c_int, c_uint, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::protocol::wl_surface_mod::wl_surface;
use client::util::wl_fixed_t;
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_pointer_interface: wl_interface;
}

pub type wl_pointer_button_state = c_uint;
pub const WL_POINTER_BUTTON_STATE_RELEASED: wl_pointer_button_state = 0;
pub const WL_POINTER_BUTTON_STATE_PRESSED: wl_pointer_button_state = 1;

pub type wl_pointer_axis = c_uint;
pub const WL_POINTER_AXIS_VERTICAL_SCROLL: wl_pointer_axis = 0;
pub const WL_POINTER_AXIS_HORIZONTAL_SCROLL: wl_pointer_axis = 1;


#[repr(C)]
pub struct wl_pointer;
impl Copy for wl_pointer {}

#[repr(C)]
pub struct wl_pointer_listener {
    pub enter: Option<extern "C" fn(data: *mut c_void, wl_pointer: *mut wl_pointer, serial: u32, surface: *mut wl_surface, surface_x: wl_fixed_t, surface_y: wl_fixed_t) -> ()>,
    pub leave: Option<extern "C" fn(data: *mut c_void, wl_pointer: *mut wl_pointer, serial: u32, surface: *mut wl_surface) -> ()>,
    pub motion: Option<extern "C" fn(data: *mut c_void, wl_pointer: *mut wl_pointer, time: u32, surface_x: wl_fixed_t, surface_y: wl_fixed_t) -> ()>,
    pub button: Option<extern "C" fn(data: *mut c_void, wl_pointer: *mut wl_pointer, serial: u32, time: u32, button: u32, state: u32) -> ()>,
    pub axis: Option<extern "C" fn(data: *mut c_void, wl_pointer: *mut wl_pointer, time: u32, axis: u32, value: wl_fixed_t) -> ()>,
}
impl Copy for wl_pointer_listener {}
impl default::Default for wl_pointer_listener {
    fn default() -> wl_pointer_listener {
        unsafe { mem::zeroed() }
    }
}

#[inline]
pub unsafe fn wl_pointer_add_listener(pointer: *mut wl_pointer, listener: *const wl_pointer_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(pointer as *mut wl_proxy, mem::transmute(listener), data)
}

const WL_POINTER_SET_CURSOR: u32 = 0;
const WL_POINTER_RELEASE: u32 = 1;

#[inline]
pub unsafe fn wl_pointer_set_user_data(pointer: *mut wl_pointer, user_data: *mut c_void) {
    wl_proxy_set_user_data(pointer as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_pointer_get_user_data(pointer: *mut wl_pointer) -> *mut c_void {
    wl_proxy_get_user_data(pointer as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_pointer_destroy(pointer: *mut wl_pointer) {
    wl_proxy_destroy(pointer as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_pointer_set_cursor(pointer: *mut wl_pointer, serial: i32, surface: *mut wl_surface, hotspot_x: i32, hotspot_y: i32) {
    wl_proxy_marshal(pointer as *mut wl_proxy, WL_POINTER_SET_CURSOR, serial, surface, hotspot_x, hotspot_y);
}

#[inline]
pub unsafe fn wl_pointer_release(pointer: *mut wl_pointer) {
    wl_proxy_marshal(pointer as *mut wl_proxy, WL_POINTER_RELEASE);
    wl_proxy_destroy(pointer as *mut wl_proxy);
}
