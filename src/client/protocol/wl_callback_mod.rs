use std::{default, mem};
use libc::{c_int, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_destroy};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_callback_interface: wl_interface;
}


#[repr(C)]
pub struct wl_callback;
impl Copy for wl_callback {}

#[repr(C)]
pub struct wl_callback_listener {
    pub done: Option<extern "C" fn (data: *mut c_void, wl_callback: *mut wl_callback, callback_data: u32) -> ()>,
}
impl Copy for wl_callback_listener {}
impl default::Default for wl_callback_listener {
    fn default() -> wl_callback_listener {
        unsafe { mem::zeroed() }
    }
}


#[inline]
pub unsafe fn wl_callback_add_listener(callback: *mut wl_callback, listener: *const wl_callback_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(callback as *mut wl_proxy, mem::transmute(listener), data)
}

#[inline]
pub unsafe fn wl_callback_set_user_data(callback: *mut wl_callback, user_data: *mut c_void) {
    wl_proxy_set_user_data(callback as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_callback_get_user_data(callback: *mut wl_callback) -> *mut c_void {
    wl_proxy_get_user_data(callback as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_callback_destroy(callback: *mut wl_callback) {
    wl_proxy_destroy(callback as *mut wl_proxy);
}
