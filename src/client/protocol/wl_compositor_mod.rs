use std::ptr;
use libc::c_void;
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_destroy, wl_proxy_marshal_constructor};
use client::protocol::wl_surface_mod::{wl_surface, wl_surface_interface};
use client::protocol::wl_region_mod::{wl_region, wl_region_interface};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_compositor_interface: wl_interface;
}

const WL_COMPOSITOR_CREATE_SURFACE: u32 = 0;
const WL_COMPOSITOR_CREATE_REGION: u32 = 1;


#[repr(C)]
pub struct wl_compositor;
impl Copy for wl_compositor {}


#[inline]
pub unsafe fn wl_compositor_set_user_data(compositor: *mut wl_compositor, user_data: *mut c_void) {
    wl_proxy_set_user_data(compositor as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_compositor_get_user_data(compositor: *mut wl_compositor) -> *mut c_void {
    wl_proxy_get_user_data(compositor as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_compositor_destroy(compositor: *mut wl_compositor) {
    wl_proxy_destroy(compositor as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_compositor_create_surface(compositor: *mut wl_compositor) -> *mut wl_surface {
    wl_proxy_marshal_constructor(compositor as *mut wl_proxy, WL_COMPOSITOR_CREATE_SURFACE, &wl_surface_interface, ptr::null_mut::<usize>()) as *mut wl_surface
}

#[inline]
pub unsafe fn wl_compositor_create_region(compositor: *mut wl_compositor) -> *mut wl_region {
    wl_proxy_marshal_constructor(compositor as *mut wl_proxy, WL_COMPOSITOR_CREATE_REGION, &wl_region_interface, ptr::null_mut::<usize>()) as *mut wl_region
}
