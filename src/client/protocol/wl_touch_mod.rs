use std::{default, mem};
use libc::{c_int, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::protocol::wl_surface_mod::wl_surface;
use client::util::{wl_interface, wl_fixed_t};


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_touch_interface: wl_interface;
}


#[repr(C)]
pub struct wl_touch;
impl Copy for wl_touch {}

#[repr(C)]
pub struct wl_touch_listener {
    pub down: Option<extern "C" fn(data: *mut c_void, wl_touch: *mut wl_touch, serial: u32, time: u32, surface: *mut wl_surface, id: i32, x: wl_fixed_t, y: wl_fixed_t) -> ()>,
    pub up: Option<extern "C" fn(data: *mut c_void, wl_touch: *mut wl_touch, serial: u32, time: u32, id: i32) -> ()>,
    pub motion: Option<extern "C" fn (data: *mut c_void, wl_touch: *mut wl_touch, time: u32, id: i32, x: wl_fixed_t, y: wl_fixed_t) -> ()>,
    pub frame: Option<extern "C" fn (data: *mut c_void, wl_touch: *mut wl_touch) -> ()>,
    pub cancel: Option<extern "C" fn  (data: *mut c_void, wl_touch: *mut wl_touch) -> ()>,
}
impl Copy for wl_touch_listener {}
impl default::Default for wl_touch_listener {
    fn default() -> wl_touch_listener {
        unsafe { mem::zeroed() }
    }
}


#[inline]
pub unsafe fn wl_touch_add_listener(touch: *mut wl_touch, listener: *const wl_touch_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(touch as *mut wl_proxy, mem::transmute(listener), data)
}

const WL_TOUCH_RELEASE: u32 = 0;

#[inline]
pub unsafe fn wl_touch_set_user_data(touch: *mut wl_touch, user_data: *mut c_void) {
    wl_proxy_set_user_data(touch as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_touch_get_user_data(touch: *mut wl_touch) -> *mut c_void {
    wl_proxy_get_user_data(touch as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_touch_destroy(touch: *mut wl_touch) {
    wl_proxy_destroy(touch as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_touch_release(touch: *mut wl_touch) {
    wl_proxy_marshal(touch as *mut wl_proxy, WL_TOUCH_RELEASE);
    wl_proxy_destroy(touch as *mut wl_proxy);
}
