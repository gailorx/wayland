use std::ptr;
use libc::{c_uint, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal, wl_proxy_marshal_constructor, wl_proxy_destroy};
use client::protocol::wl_surface_mod::wl_surface;
use client::protocol::wl_subsurface_mod::{wl_subsurface, wl_subsurface_interface};
use client::util::wl_interface;

#[link(name = "wayland-client")]
extern "C" {
    pub static wl_subcompositor_interface: wl_interface;
}


pub type wl_subcompositor_error = c_uint;
pub const WL_SUBCOMPOSITOR_ERROR_BAD_SURFACE: wl_subcompositor_error = 0;

const WL_SUBCOMPOSITOR_DESTROY: u32 = 0;
const WL_SUBCOMPOSITOR_GET_SUBSURFACE: u32 = 1;

#[repr(C)]
pub struct wl_subcompositor;
impl Copy for wl_subcompositor {}

#[inline]
pub unsafe fn wl_subcompositor_set_user_data(subcompositor: *mut wl_subcompositor, user_data: *mut c_void) {
    wl_proxy_set_user_data(subcompositor as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_subcompositor_get_user_data(subcompositor: *mut wl_subcompositor) -> *mut c_void {
    wl_proxy_get_user_data(subcompositor as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_subcompositor_destroy(subcompositor: *mut wl_subcompositor) {
    wl_proxy_marshal(subcompositor as *mut wl_proxy, WL_SUBCOMPOSITOR_DESTROY);
    wl_proxy_destroy(subcompositor as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_subcompositor_get_subsurface(subcompositor: *mut wl_subcompositor, surface: *mut wl_surface, parent: *mut wl_surface) -> *mut wl_subsurface {
    wl_proxy_marshal_constructor(subcompositor as *mut wl_proxy, WL_SUBCOMPOSITOR_GET_SUBSURFACE, &wl_subsurface_interface, ptr::null_mut::<usize>(), surface, parent) as *mut wl_subsurface
}
