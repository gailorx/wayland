use std::{default, ptr, mem};
use libc::{c_int, c_char, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal_constructor, wl_proxy_destroy};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_registry_interface: wl_interface;
}

#[repr(C)]
pub struct wl_registry;
impl Copy for wl_registry {}

#[repr(C)]
pub struct wl_registry_listener {
    pub global: Option<extern "C" fn(data: *mut c_void, wl_registry: *mut wl_registry, name: u32, interface: *const c_char, version: u32) -> ()>,
    pub global_remove: Option<extern "C" fn(data: *mut c_void, wl_registry: *mut wl_registry, name: u32) -> ()>,
}
impl Copy for wl_registry_listener {}
impl default::Default for wl_registry_listener {
    fn default() -> wl_registry_listener {
        unsafe { mem::zeroed() }
    }
}


#[inline]
pub unsafe fn wl_registry_add_listener(registry: *mut wl_registry, listener: *const wl_registry_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(registry as *mut wl_proxy, mem::transmute(listener), data)
}

const WL_REGISTRY_BIND: u32 = 0;

#[inline]
pub unsafe fn wl_registry_set_user_data(registry: *mut wl_registry, user_data: *mut c_void) {
    wl_proxy_set_user_data(registry as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_registry_get_user_data(registry: *mut wl_registry) -> *mut c_void {
    wl_proxy_get_user_data(registry as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_registry_destroy(registry: *mut wl_registry) {
    wl_proxy_destroy(registry as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_registry_bind(registry: *mut wl_registry, name: u32, interface: *const wl_interface, version: u32) -> *mut c_void {
    wl_proxy_marshal_constructor(registry as *mut wl_proxy, WL_REGISTRY_BIND, interface, name, (*interface).name, version, ptr::null_mut::<usize>()) as *mut c_void
}
