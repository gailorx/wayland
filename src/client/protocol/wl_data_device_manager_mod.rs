use std::ptr;
use libc::c_void;
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_destroy, wl_proxy_marshal_constructor};
use client::protocol::wl_data_source_mod::{wl_data_source, wl_data_source_interface};
use client::protocol::wl_seat_mod::wl_seat;
use client::protocol::wl_data_device_mod::{wl_data_device, wl_data_device_interface};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_data_device_manager_interface: wl_interface;
}


const WL_DATA_DEVICE_MANAGER_CREATE_DATA_SOURCE: u32 = 0;
const WL_DATA_DEVICE_MANAGER_GET_DATA_DEVICE: u32 = 1;


#[repr(C)]
pub struct wl_data_device_manager;
impl Copy for wl_data_device_manager {}


#[inline]
pub unsafe fn wl_data_device_manager_set_user_data(data_device_manager: *mut wl_data_device_manager, user_data: *mut c_void) {
    wl_proxy_set_user_data(data_device_manager as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_data_device_manager_get_user_data(data_device_manager: *mut wl_data_device_manager) -> *mut c_void {
    wl_proxy_get_user_data(data_device_manager as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_data_device_manager_destroy(data_device_manager: *mut wl_data_device_manager) {
    wl_proxy_destroy(data_device_manager as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_data_device_manager_create_data_source(data_device_manager: *mut wl_data_device_manager) -> *mut wl_data_source {
    wl_proxy_marshal_constructor(data_device_manager as *mut wl_proxy, WL_DATA_DEVICE_MANAGER_CREATE_DATA_SOURCE, &wl_data_source_interface, ptr::null_mut::<usize>()) as *mut wl_data_source
}

#[inline]
pub unsafe fn wl_data_device_manager_get_data_device(data_device_manager: *mut wl_data_device_manager, seat: *mut wl_seat) -> *mut wl_data_device {
    wl_proxy_marshal_constructor(data_device_manager as *mut wl_proxy, WL_DATA_DEVICE_MANAGER_GET_DATA_DEVICE, &wl_data_device_interface, ptr::null_mut::<usize>(), seat) as *mut wl_data_device
}
