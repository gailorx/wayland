use std::{default, ptr, mem};
use libc::{c_int, c_uint, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal_constructor, wl_proxy_destroy};
use client::protocol::wl_shm_pool_mod::{wl_shm_pool, wl_shm_pool_interface};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_shm_interface: wl_interface;
}

pub type wl_shm_error = c_uint;
pub const WL_SHM_ERROR_INVALID_FORMAT: wl_shm_error = 0;
pub const WL_SHM_ERROR_INVALID_STRIDE: wl_shm_error = 1;
pub const WL_SHM_ERROR_INVALID_FD: wl_shm_error = 2;

pub type wl_shm_format = c_uint;
pub const WL_SHM_FORMAT_ARGB8888: wl_shm_format = 0;
pub const WL_SHM_FORMAT_XRGB8888: wl_shm_format = 1;
pub const WL_SHM_FORMAT_C8: wl_shm_format = 538982467;
pub const WL_SHM_FORMAT_RGB332: wl_shm_format = 943867730;
pub const WL_SHM_FORMAT_BGR233: wl_shm_format = 944916290;
pub const WL_SHM_FORMAT_XRGB4444: wl_shm_format = 842093144;
pub const WL_SHM_FORMAT_XBGR4444: wl_shm_format = 842089048;
pub const WL_SHM_FORMAT_RGBX4444: wl_shm_format = 842094674;
pub const WL_SHM_FORMAT_BGRX4444: wl_shm_format = 842094658;
pub const WL_SHM_FORMAT_ARGB4444: wl_shm_format = 842093121;
pub const WL_SHM_FORMAT_ABGR4444: wl_shm_format = 842089025;
pub const WL_SHM_FORMAT_RGBA4444: wl_shm_format = 842088786;
pub const WL_SHM_FORMAT_BGRA4444: wl_shm_format = 842088770;
pub const WL_SHM_FORMAT_XRGB1555: wl_shm_format = 892424792;
pub const WL_SHM_FORMAT_XBGR1555: wl_shm_format = 892420696;
pub const WL_SHM_FORMAT_RGBX5551: wl_shm_format = 892426322;
pub const WL_SHM_FORMAT_BGRX5551: wl_shm_format = 892426306;
pub const WL_SHM_FORMAT_ARGB1555: wl_shm_format = 892424769;
pub const WL_SHM_FORMAT_ABGR1555: wl_shm_format = 892420673;
pub const WL_SHM_FORMAT_RGBA5551: wl_shm_format = 892420434;
pub const WL_SHM_FORMAT_BGRA5551: wl_shm_format = 892420418;
pub const WL_SHM_FORMAT_RGB565: wl_shm_format = 909199186;
pub const WL_SHM_FORMAT_BGR565: wl_shm_format = 909199170;
pub const WL_SHM_FORMAT_RGB888: wl_shm_format = 875710290;
pub const WL_SHM_FORMAT_BGR888: wl_shm_format = 875710274;
pub const WL_SHM_FORMAT_XBGR8888: wl_shm_format = 875709016;
pub const WL_SHM_FORMAT_RGBX8888: wl_shm_format = 875714642;
pub const WL_SHM_FORMAT_BGRX8888: wl_shm_format = 875714626;
pub const WL_SHM_FORMAT_ABGR8888: wl_shm_format = 875708993;
pub const WL_SHM_FORMAT_RGBA8888: wl_shm_format = 875708754;
pub const WL_SHM_FORMAT_BGRA8888: wl_shm_format = 875708738;
pub const WL_SHM_FORMAT_XRGB2101010: wl_shm_format = 808669784;
pub const WL_SHM_FORMAT_XBGR2101010: wl_shm_format = 808665688;
pub const WL_SHM_FORMAT_RGBX1010102: wl_shm_format = 808671314;
pub const WL_SHM_FORMAT_BGRX1010102: wl_shm_format = 808671298;
pub const WL_SHM_FORMAT_ARGB2101010: wl_shm_format = 808669761;
pub const WL_SHM_FORMAT_ABGR2101010: wl_shm_format = 808665665;
pub const WL_SHM_FORMAT_RGBA1010102: wl_shm_format = 808665426;
pub const WL_SHM_FORMAT_BGRA1010102: wl_shm_format = 808665410;
pub const WL_SHM_FORMAT_YUYV: wl_shm_format = 1448695129;
pub const WL_SHM_FORMAT_YVYU: wl_shm_format = 1431918169;
pub const WL_SHM_FORMAT_UYVY: wl_shm_format = 1498831189;
pub const WL_SHM_FORMAT_VYUY: wl_shm_format = 1498765654;
pub const WL_SHM_FORMAT_AYUV: wl_shm_format = 1448433985;
pub const WL_SHM_FORMAT_NV12: wl_shm_format = 842094158;
pub const WL_SHM_FORMAT_NV21: wl_shm_format = 825382478;
pub const WL_SHM_FORMAT_NV16: wl_shm_format = 909203022;
pub const WL_SHM_FORMAT_NV61: wl_shm_format = 825644622;
pub const WL_SHM_FORMAT_YUV410: wl_shm_format = 961959257;
pub const WL_SHM_FORMAT_YVU410: wl_shm_format = 961893977;
pub const WL_SHM_FORMAT_YUV411: wl_shm_format = 825316697;
pub const WL_SHM_FORMAT_YVU411: wl_shm_format = 825316953;
pub const WL_SHM_FORMAT_YUV420: wl_shm_format = 842093913;
pub const WL_SHM_FORMAT_YVU420: wl_shm_format = 842094169;
pub const WL_SHM_FORMAT_YUV422: wl_shm_format = 909202777;
pub const WL_SHM_FORMAT_YVU422: wl_shm_format = 909203033;
pub const WL_SHM_FORMAT_YUV444: wl_shm_format = 875713881;
pub const WL_SHM_FORMAT_YVU444: wl_shm_format = 875714137;


const WL_SHM_CREATE_POOL: u32 = 0;


#[repr(C)]
pub struct wl_shm;
impl Copy for wl_shm {}


#[repr(C)]
pub struct wl_shm_listener {
    pub format: Option<extern "C" fn(data: *mut c_void, wl_shm: *mut wl_shm, format: u32) -> ()>,
}
impl Copy for wl_shm_listener {}
impl default::Default for wl_shm_listener {
    fn default() -> wl_shm_listener { unsafe { mem::zeroed() } }
}


#[inline]
pub unsafe fn wl_shm_add_listener(shm: *mut wl_shm, listener: *const wl_shm_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(shm as *mut wl_proxy, mem::transmute(listener), data)
}

#[inline]
pub unsafe fn wl_shm_set_user_data(shm: *mut wl_shm, user_data: *mut c_void) {
    wl_proxy_set_user_data(shm as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_shm_get_user_data(shm: *mut wl_shm) -> *mut c_void {
    wl_proxy_get_user_data(shm as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_shm_destroy(shm: *mut wl_shm) {
    wl_proxy_destroy(shm as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_shm_create_pool(shm: *mut wl_shm, fd: i32, size: i32) -> *mut wl_shm_pool {
    wl_proxy_marshal_constructor(shm as *mut wl_proxy, WL_SHM_CREATE_POOL, &wl_shm_pool_interface, ptr::null_mut::<usize>(), fd, size) as *mut wl_shm_pool
}
