use std::ptr;
use libc::c_void;
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal_constructor, wl_proxy_destroy};
use client::protocol::wl_shell_surface_mod::{wl_shell_surface, wl_shell_surface_interface};
use client::protocol::wl_surface_mod::wl_surface;
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_shell_interface: wl_interface;
}

const WL_SHELL_GET_SHELL_SURFACE: u32 = 0;

#[repr(C)]
pub struct wl_shell;
impl Copy for wl_shell {}


#[inline]
pub unsafe fn wl_shell_set_user_data(shell: *mut wl_shell, user_data: *mut c_void) {
    wl_proxy_set_user_data(shell as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_shell_get_user_data(shell: *mut wl_shell) -> *mut c_void {
    wl_proxy_get_user_data(shell as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_shell_destroy(shell: *mut wl_shell) {
    wl_proxy_destroy(shell as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_shell_get_shell_surface(shell: *mut wl_shell, surface: *mut wl_surface) -> *mut wl_shell_surface {
    wl_proxy_marshal_constructor(shell as *mut wl_proxy, WL_SHELL_GET_SHELL_SURFACE, &wl_shell_surface_interface, ptr::null_mut::<usize>(), surface) as *mut wl_shell_surface
}
