use libc::{c_uint, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::protocol::wl_surface_mod::wl_surface;
use client::util::wl_interface;

#[link(name = "wayland-client")]
extern "C" {
    pub static wl_subsurface_interface: wl_interface;
}

pub type wl_subsurface_error = c_uint;
pub const WL_SUBSURFACE_ERROR_BAD_SURFACE: wl_subsurface_error = 0;

const WL_SUBSURFACE_DESTROY: u32 = 0;
const WL_SUBSURFACE_SET_POSITION: u32 = 1;
const WL_SUBSURFACE_PLACE_ABOVE: u32 = 2;
const WL_SUBSURFACE_PLACE_BELOW: u32 = 3;
const WL_SUBSURFACE_SET_SYNC: u32 = 4;
const WL_SUBSURFACE_SET_DESYNC: u32 = 5;


#[repr(C)]
pub struct wl_subsurface;
impl Copy for wl_subsurface {}


#[inline]
pub unsafe fn wl_subsurface_set_user_data(surface: *mut wl_surface, user_data: *mut c_void) {
    wl_proxy_set_user_data(surface as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_subsurface_get_user_data(surface: *mut wl_surface) -> *mut c_void {
    wl_proxy_get_user_data(surface as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_subsurface_destroy(surface: *mut wl_surface) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SUBSURFACE_DESTROY);
    wl_proxy_destroy(surface as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_subsurface_set_position(surface: *mut wl_surface, x: i32, y: i32) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SUBSURFACE_SET_POSITION, x, y);
}

#[inline]
pub unsafe fn wl_subsurface_place_above(surface: *mut wl_surface, sibling: *mut wl_surface) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SUBSURFACE_PLACE_ABOVE, sibling);
}

#[inline]
pub unsafe fn wl_subsurface_place_below(surface: *mut wl_surface, sibling: *mut wl_surface) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SUBSURFACE_PLACE_BELOW, sibling);
}

#[inline]
pub unsafe fn wl_subsurface_set_sync(surface: *mut wl_surface) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SUBSURFACE_SET_SYNC);
}

#[inline]
pub unsafe fn wl_subsurface_set_desync(surface: *mut wl_surface) {
    wl_proxy_marshal(surface as *mut wl_proxy, WL_SUBSURFACE_SET_DESYNC);
}
