use std::{default, mem};
use libc::{c_int, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal, wl_proxy_destroy};
use client::util::wl_interface;


#[link(name = "wayland-client")]
extern "C" {
    pub static wl_buffer_interface: wl_interface;
}

const WL_BUFFER_DESTROY: u32 = 0;


#[repr(C)]
pub struct wl_buffer;
impl Copy for wl_buffer {}

#[repr(C)]
pub struct wl_buffer_listener {
    pub release: Option<extern "C" fn(data: *mut c_void, wl_buffer: *mut wl_buffer)   -> ()>,
}
impl Copy for wl_buffer_listener {}
impl default::Default for wl_buffer_listener {
    fn default() -> wl_buffer_listener {
        unsafe { mem::zeroed() }
    }
}

#[inline]
pub unsafe fn wl_buffer_add_listener(buffer: *mut wl_buffer, listener: *const wl_buffer_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(buffer as *mut wl_proxy, mem::transmute(listener), data)
}

#[inline]
pub unsafe fn wl_buffer_set_user_data(buffer: *mut wl_buffer, user_data: *mut c_void) {
    wl_proxy_set_user_data(buffer as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_buffer_get_user_data(buffer: *mut wl_buffer) -> *mut c_void {
    wl_proxy_get_user_data(buffer as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_buffer_destroy(buffer: *mut wl_buffer) {
    wl_proxy_marshal(buffer as *mut wl_proxy, WL_BUFFER_DESTROY);
    wl_proxy_destroy(buffer as *mut wl_proxy);
}
