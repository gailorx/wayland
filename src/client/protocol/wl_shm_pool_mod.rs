use std::ptr;
use libc::c_void;
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal, wl_proxy_marshal_constructor, wl_proxy_destroy};
use client::protocol::wl_buffer_mod::{wl_buffer, wl_buffer_interface};
use client::util::wl_interface;

#[link(name = "wayland-client")]
extern "C" {
    pub static wl_shm_pool_interface: wl_interface;
}

const WL_SHM_POOL_CREATE_BUFFER: u32 = 0;
const WL_SHM_POOL_DESTROY: u32 = 1;
const WL_SHM_POOL_RESIZE: u32 = 2;

#[repr(C)]
pub struct wl_shm_pool;
impl Copy for wl_shm_pool {}


#[inline]
pub unsafe fn wl_shm_pool_set_user_data(shm_pool: *mut wl_shm_pool, user_data: *mut c_void) {
    wl_proxy_set_user_data(shm_pool as *mut wl_proxy, user_data)
}

#[inline]
pub unsafe fn wl_shm_pool_get_user_data(shm_pool: *mut wl_shm_pool) -> *mut c_void {
    wl_proxy_get_user_data(shm_pool as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_shm_pool_create_buffer(shm_pool: *mut wl_shm_pool, offset: i32, width: i32, height: i32, stride: i32, format: u32) -> *mut wl_buffer {
    wl_proxy_marshal_constructor(shm_pool as *mut wl_proxy, WL_SHM_POOL_CREATE_BUFFER, &wl_buffer_interface, ptr::null_mut::<usize>(), offset, width, height, stride, format) as *mut wl_buffer
}

#[inline]
pub unsafe fn wl_shm_pool_destroy(shm_pool: *mut wl_shm_pool) {
    wl_proxy_marshal(shm_pool as *mut wl_proxy, WL_SHM_POOL_DESTROY);
    wl_proxy_destroy(shm_pool as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_shm_pool_resize(shm_pool: *mut wl_shm_pool, size: i32) {
    wl_proxy_marshal(shm_pool as *mut wl_proxy, WL_SHM_POOL_RESIZE, size);
}
