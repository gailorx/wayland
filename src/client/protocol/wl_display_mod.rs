use std::{default, ptr, mem};
use libc::{c_int, c_uint, c_char, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_set_user_data, wl_proxy_get_user_data, wl_proxy_marshal_constructor};
use client::protocol::wl_callback_mod::{wl_callback, wl_callback_interface};
use client::protocol::wl_registry_mod::{wl_registry, wl_registry_interface};
use client::protocol::wl_event_queue_mod::wl_event_queue;
use client::util::wl_interface;

#[link(name = "wayland-client")]
extern "C" {
    pub static wl_display_interface: wl_interface;
}


pub type wl_display_error = c_uint;
pub const WL_DISPLAY_ERROR_INVALID_OBJECT: wl_display_error = 0;
pub const WL_DISPLAY_ERROR_INVALID_METHOD: wl_display_error = 1;
pub const WL_DISPLAY_ERROR_NO_MEMORY: wl_display_error = 2;


const WL_DISPLAY_SYNC: u32 = 0;
const WL_DISPLAY_GET_REGISTRY: u32 = 1;


#[repr(C)]
pub struct wl_display;
impl Copy for wl_display {}

#[repr(C)]
pub struct wl_display_listener {
    pub error: Option<extern "C" fn(data: *mut c_void,  wl_display: *mut wl_display, object_id: *mut c_void, code: u32, message: *const c_char) -> ()>,
    pub delete_id: Option<extern "C" fn(data: *mut c_void, wl_display: *mut wl_display, id: u32) -> ()>,
}
impl Copy for wl_display_listener {}
impl default::Default for wl_display_listener {
    fn default() -> wl_display_listener {
        unsafe { mem::zeroed() }
    }
}



#[inline]
pub unsafe fn wl_display_add_listener(display: *mut wl_display, listener: *const wl_display_listener, data: *mut c_void) -> c_int {
    return wl_proxy_add_listener(display as *mut wl_proxy, mem::transmute(listener), data);
}


#[inline]
pub unsafe fn wl_display_set_user_data(display: *mut wl_display, user_data: *mut c_void) {
    wl_proxy_set_user_data(display as *mut wl_proxy, user_data)
}

#[inline]
pub unsafe fn wl_display_get_user_data(display: *mut wl_display) -> *mut c_void {
    wl_proxy_get_user_data(display as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_display_sync(display: *mut wl_display) -> *mut wl_callback {
    wl_proxy_marshal_constructor(display as *mut wl_proxy, WL_DISPLAY_SYNC, &wl_callback_interface, ptr::null_mut::<usize>()) as *mut wl_callback
}

#[inline]
pub unsafe fn wl_display_get_registry(display: *mut wl_display) -> *mut wl_registry {
    wl_proxy_marshal_constructor(display as *mut wl_proxy, WL_DISPLAY_GET_REGISTRY, &wl_registry_interface, ptr::null_mut::<usize>()) as *mut wl_registry
}


#[link(name = "wayland-client")]
extern "C" {
    pub fn wl_display_connect(name: *const c_char) -> *mut wl_display;
    pub fn wl_display_connect_to_fd(fd: c_int) -> *mut wl_display;
    pub fn wl_display_disconnect(display: *mut wl_display);
    pub fn wl_display_get_fd(display: *mut wl_display) -> c_int;
    pub fn wl_display_dispatch(display: *mut wl_display) -> c_int;
    pub fn wl_display_dispatch_queue(display: *mut wl_display, queue: *mut wl_event_queue) -> c_int;
    pub fn wl_display_dispatch_queue_pending(display: *mut wl_display, queue: *mut wl_event_queue) -> c_int;
    pub fn wl_display_dispatch_pending(display: *mut wl_display) -> c_int;
    pub fn wl_display_get_error(display: *mut wl_display) -> c_int;
    pub fn wl_display_get_protocol_error(display: *mut wl_display, interface: *mut *const wl_interface, id: *mut u32) -> u32;
    pub fn wl_display_flush(display: *mut wl_display) -> c_int;
    pub fn wl_display_roundtrip_queue(display: *mut wl_display, queue: *mut wl_event_queue) -> c_int;
    pub fn wl_display_roundtrip(display: *mut wl_display) -> c_int;
    pub fn wl_display_create_queue(display: *mut wl_display) -> *mut wl_event_queue;
    pub fn wl_display_prepare_read_queue(display: *mut wl_display, queue: *mut wl_event_queue) -> c_int;
    pub fn wl_display_prepare_read(display: *mut wl_display) -> c_int;
    pub fn wl_display_cancel_read(display: *mut wl_display);
    pub fn wl_display_read_events(display: *mut wl_display) -> c_int;
}
