use std::{default, ptr, mem};
use libc::{c_int, c_uint, c_char, c_void};
use client::protocol::wl_proxy_mod::{wl_proxy, wl_proxy_add_listener, wl_proxy_get_user_data, wl_proxy_set_user_data, wl_proxy_marshal_constructor, wl_proxy_destroy};
use client::protocol::wl_pointer_mod::{wl_pointer, wl_pointer_interface};
use client::protocol::wl_keyboard_mod::{wl_keyboard, wl_keyboard_interface};
use client::protocol::wl_touch_mod::{wl_touch, wl_touch_interface};
use client::util::wl_interface;

#[link(name = "wayland-client")]
extern "C" {
    pub static wl_seat_interface: wl_interface;
}


pub type wl_seat_capability = c_uint;
pub const WL_SEAT_CAPABILITY_POINTER: wl_seat_capability = 1;
pub const WL_SEAT_CAPABILITY_KEYBOARD: wl_seat_capability = 2;
pub const WL_SEAT_CAPABILITY_TOUCH: wl_seat_capability = 4;


const WL_SEAT_GET_POINTER: u32 = 0;
const WL_SEAT_GET_KEYBOARD: u32 = 1;
const WL_SEAT_GET_TOUCH: u32 = 2;


#[repr(C)]
pub struct wl_seat;
impl Copy for wl_seat {}


#[repr(C)]
pub struct wl_seat_listener {
    pub capabilities: Option<extern "C" fn(data: *mut c_void, wl_seat: *mut wl_seat, capabilities: u32) -> ()>,
    pub name: Option<extern "C" fn(data: *mut c_void, wl_seat: *mut wl_seat, name: *const c_char) -> ()>,
}
impl Copy for wl_seat_listener {}
impl default::Default for wl_seat_listener {
    fn default() -> wl_seat_listener {
        unsafe { mem::zeroed() }
    }
}

#[inline]
pub unsafe fn wl_seat_add_listener(seat: *mut wl_seat, listener: *const wl_seat_listener, data: *mut c_void) -> c_int {
    wl_proxy_add_listener(seat as *mut wl_proxy, mem::transmute(listener), data)
}


#[inline]
pub unsafe fn wl_seat_set_user_data(seat: *mut wl_seat, user_data: *mut c_void) {
    wl_proxy_set_user_data(seat as *mut wl_proxy, user_data);
}

#[inline]
pub unsafe fn wl_seat_get_user_data(seat: *mut wl_seat) -> *mut c_void {
    wl_proxy_get_user_data(seat as *mut wl_proxy)
}

#[inline]
pub unsafe fn wl_seat_destroy(seat: *mut wl_seat) {
    wl_proxy_destroy(seat as *mut wl_proxy);
}

#[inline]
pub unsafe fn wl_seat_get_pointer(seat: *mut wl_seat) -> *mut wl_pointer {
    wl_proxy_marshal_constructor(seat as *mut wl_proxy, WL_SEAT_GET_POINTER, &wl_pointer_interface, ptr::null_mut::<usize>()) as *mut wl_pointer
}

#[inline]
pub unsafe fn wl_seat_get_keyboard(seat: *mut wl_seat) -> *mut wl_keyboard {
    wl_proxy_marshal_constructor(seat as *mut wl_proxy, WL_SEAT_GET_KEYBOARD, &wl_keyboard_interface, ptr::null_mut::<usize>()) as *mut wl_keyboard
}

#[inline]
pub unsafe fn wl_seat_get_touch(seat: *mut wl_seat) -> *mut wl_touch {
    wl_proxy_marshal_constructor(seat as *mut wl_proxy, WL_SEAT_GET_TOUCH, &wl_touch_interface, ptr::null_mut::<usize>()) as *mut wl_touch
}
